(ns monty-hall.core
  (:gen-class))

; init doors
(defn init-doors
  "Initializes a map of doors. One door will have a car. The other two doors will have goats."
  []
  (let [doors [:car :goat :goat]]
    (zipmap (range 1 4) (shuffle doors))))

(defn pick-door
  "Picks a door at random from a map of doors and returns its key value."
  [doors]
  (let [door-numbers (keys doors)]
    (rand-nth door-numbers)))

(defn reveal-goat-door
  "Given a map of doors and a door key that the contestant has chosen, this function returns the key for a remaining door which has a goat."
  [doors door-choice]
  (let [remaining-doors (dissoc doors door-choice)
        goat-doors (filter #(= :goat (second %)) remaining-doors)]
    (first (rand-nth goat-doors))))

(defn find-car-door
  "Returns the key for the car door given a map of doors."
  [doors]
  (->> doors
      (filter #(= :car (val %)))
       ffirst))

(defn play-game
  "Runs the monty-hall game. If is-switch is truthy, the contestant will change their original door choice."
  [is-switch]
  (let [doors (init-doors)
        initial-door-choice (pick-door doors)
        car-door (find-car-door doors)]
    (if is-switch
      (let [remaining-doors (dissoc doors (reveal-goat-door doors initial-door-choice))
            switch-door (first (map key (dissoc remaining-doors initial-door-choice)))]
        (= switch-door car-door))
      (= initial-door-choice car-door))))

(defn run-trials
  "Runs num-trials trials with door swittching iff switch-door is truthy. Returns the win ratio as a float."
  [num-trials switch-door]
  (let [success-count (->> #(play-game switch-door)
                           repeatedly
                           (take num-trials)
                           (filter true?)
                           count)]
    (float (/ success-count num-trials))))

(defn run-split-trials
  "Runs num-trials trials once with door switching and once without. Prints the success raio of each to the console."
  [num-trials]
  (let [switch-success-ratio (run-trials num-trials true)
        no-switch-trials (run-trials num-trials false)]
    (println "Success ratio with switching: " switch-success-ratio)
    (println "Success ratio with no switching: " no-switch-trials)))

(defn -main
  "Accepts an integer, n, runs a Monty-Hall simulation of n trials, and prints the results."
  [& args]
  (run-split-trials (Integer/parseInt (first args))))
