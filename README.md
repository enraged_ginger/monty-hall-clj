# monty-hall

A Clojure application for running Monty Hall simulations.

## Usage

Run from the command line using leiningen. This program accepts only one parameter: the number of trials to run.

```
$ lein run 10000
Success ratio with switching:  0.6701
Success ratio with no switching:  0.3342
```

## License

Copyright © 2015 Stephen M. Hopper

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.